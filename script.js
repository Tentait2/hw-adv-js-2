"use strict"

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.
// В основном конструкцию try catch нам нужно использовать когда мы ожидаем ошибку в коде. Особенно такие ошибки могут произойти когда к нам приходит информация с других источников, но мы не знаем какая именно информация

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

function createBooksList(arrayBooksList){
    let root = document.querySelector('#root')
    let list = document.createElement('ul')
    root.append(list)
    const requiredProperties = ['author', 'name', 'price']
    arrayBooksList.forEach(element => {
        let listItem = document.createElement('li')
        if (JSON.stringify(requiredProperties) === JSON.stringify(Object.keys(element))){
            listItem.textContent =`Автор ${element.author}, назва: ${element.name}, ціна:${element.price}` ;
            list.append(listItem);
        }else try{
            // Ищем отсутствующее свойство
            let missingProp = Object.keys(element).filter(prop => !requiredProperties.includes(prop)).concat(requiredProperties.filter(prop=>!Object.keys(element).includes(prop))).join()
            //Пробрасываем ошибку
            throw new Error(`${missingProp} свойство не найдено`)
        }
        catch(error){
            console.log(element);
            console.log( error.message);
        }
    });
}

createBooksList(books)

